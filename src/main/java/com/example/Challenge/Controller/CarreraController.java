package com.example.Challenge.Controller;

import com.example.Challenge.dto.CareerDto;
import com.example.Challenge.service.CareerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CarreraController {

private final CareerService careerService;

@GetMapping(value = {"/careers"},produces = "application/json")
public ResponseEntity<List<CareerDto>> getAllCareer(){

	return ResponseEntity.ok(careerService.getAllCareer());
}

@PostMapping(value = "/career",consumes = "application/json")
@ResponseStatus(HttpStatus.CREATED)
public void saveCareer(@RequestBody CareerDto CareerDto) {

	careerService.saveCareer(CareerDto);
}

@PutMapping(value = "/career", consumes = "application/json")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void updateCareer(@RequestBody CareerDto CareerDto){

	careerService.updateCareer(CareerDto);
}

@DeleteMapping("/{careerId}/career")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void downCareer(@PathVariable("careerId") Long careerId){
	careerService.downCareer(careerId);
}
}
