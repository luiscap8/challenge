package com.example.Challenge.Controller;

import com.example.Challenge.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {
	@ExceptionHandler(value = RuntimeException.class)
	public ResponseEntity<ErrorDto> runTimeExceptionHandler(RuntimeException ex) {
		ErrorDto error = ErrorDto.builder()
				.descriptionError(ex.getCause() != null ? ex.getCause().getMessage():"")
				.message(ex.getMessage())
				.build();
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
