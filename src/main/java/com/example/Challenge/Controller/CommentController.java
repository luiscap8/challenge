package com.example.Challenge.Controller;

import com.example.Challenge.dto.NewCommentDto;
import com.example.Challenge.dto.ResponseCommentDto;
import com.example.Challenge.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CommentController {

private final CommentService commentService;

@GetMapping(value = {"/comment/{ticketId}"},produces = "application/json")
public ResponseEntity<ResponseCommentDto> getComments(@PathVariable String ticketId){

	return ResponseEntity.ok(commentService.getComment(ticketId));
}

@PutMapping(value = "/comment", produces = "application/json")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void updateUser(@RequestBody NewCommentDto newCommentDto) {

	commentService.updateComment(newCommentDto);
}

}
