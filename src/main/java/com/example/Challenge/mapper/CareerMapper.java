package com.example.Challenge.mapper;

import com.example.Challenge.dto.CareerDto;
import com.example.Challenge.model.entity.Career;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CareerMapper {

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createDateTime", ignore = true)
	@Mapping(target = "updateDateTime", ignore = true)
	Career toSimpleCareer(CareerDto CareerDto);

	@Mapping(target = "createDateTime", ignore = true)
	@Mapping(target = "updateDateTime", ignore = true)
	List<CareerDto> toSimpleCareerDtoList(List<Career> CareerList);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createDateTime", ignore = true)
	@Mapping(target = "updateDateTime", ignore = true)
	Career updateCareerToSave(CareerDto CareerDto, @MappingTarget Career career);
}
