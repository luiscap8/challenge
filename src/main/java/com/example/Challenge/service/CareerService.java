package com.example.Challenge.service;

import com.example.Challenge.dto.CareerDto;
import com.example.Challenge.model.entity.Career;

import java.util.List;

public interface CareerService {
	void saveCareer(CareerDto CareerDto);
	List<CareerDto> getAllCareer();
	void updateCareer(CareerDto CareerDto) ;
	void downCareer(Long careerId);
}
