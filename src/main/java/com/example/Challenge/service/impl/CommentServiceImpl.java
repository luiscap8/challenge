package com.example.Challenge.service.impl;

import com.example.Challenge.dto.CommentToAddDto;
import com.example.Challenge.dto.NewCommentDto;
import com.example.Challenge.dto.ResponseCommentDto;
import com.example.Challenge.service.CommentService;
import com.squareup.okhttp.Credentials;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

@Value("${external.service.base.url}")
private String basePath;
@Value("${external.service.token}")
private String token;
@Value("${external.service.user}")
private String user;
private final RestTemplate restTemplate;

@Override
public ResponseCommentDto getComment(String id) {

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add(HttpHeaders.AUTHORIZATION, Credentials.basic(user, token));
    ResponseEntity<ResponseCommentDto> responseEntity = null;
    try {
        responseEntity = restTemplate.exchange(basePath + "/tickets/" + id + "/comments", HttpMethod.GET,
                new HttpEntity<>(headers), ResponseCommentDto.class);
    } catch (Exception e) {
        if (e.getMessage().contains("404"))
            throw new RuntimeException("ID not found.", e);
    }
    return responseEntity != null ? responseEntity.getBody() : null;
}

@Override
public void updateComment(NewCommentDto newCommentDto) {

    try {
        if (newCommentDto.getId() == null)
            throw new Exception();
        CommentToAddDto ticket = new CommentToAddDto(newCommentDto);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.AUTHORIZATION, Credentials.basic(user, token));
        restTemplate.exchange(basePath + "/tickets/" + newCommentDto.getId(), HttpMethod.PUT,
                new HttpEntity<>(ticket, headers), Object.class);
    } catch (Exception e) {
        throw new RuntimeException("The comment cannot be added", e);
    }
}
}
