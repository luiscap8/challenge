package com.example.Challenge.service.impl;

import com.example.Challenge.dto.CareerDto;
import com.example.Challenge.mapper.CareerMapper;
import com.example.Challenge.model.entity.Career;
import com.example.Challenge.model.repository.CareerRepository;
import com.example.Challenge.service.CareerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CareerServiceImpl implements CareerService {

private final CareerRepository careerRepository;
private final CareerMapper careerMapper;

@Override
public List<CareerDto> getAllCareer() {

    List<Career> careers = careerRepository.findAll();
    careers.removeIf(career -> career.getStatus().equals(false));
    List<CareerDto> careerDtos = careerMapper.toSimpleCareerDtoList(careers);
    return careerDtos;
}

@Transactional
@Override
public void saveCareer(CareerDto CareerDto) {

    Career careerToSave = careerMapper.toSimpleCareer(CareerDto);
    careerRepository.save(careerToSave);
}

@Transactional
@Override
public void updateCareer(CareerDto CareerDto) {

    if (CareerDto.getId() == null)
        throw new RuntimeException("Id is null");
    Career careerToSave = careerRepository.findById(CareerDto.getId())
                                  .orElseThrow(() -> new RuntimeException("No career was found with that ID."));
    Career career = careerMapper.updateCareerToSave(CareerDto, careerToSave);
    careerRepository.save(career);
}

@Transactional
@Override
public void downCareer(Long careerId) {

    if (careerId == null)
        throw new RuntimeException("Id is null");
    Career career = careerRepository.findById(careerId)
                            .orElseThrow(() -> new RuntimeException("No career was found with that ID."));
    career.setStatus(false);
    careerRepository.save(career);
}
}
