package com.example.Challenge.service;

import com.example.Challenge.dto.NewCommentDto;
import com.example.Challenge.dto.ResponseCommentDto;

public interface CommentService {

ResponseCommentDto getComment(String id);

void updateComment(NewCommentDto newCommentDto);
}
