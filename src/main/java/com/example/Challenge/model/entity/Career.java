package com.example.Challenge.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "career_ft")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Career {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
private Long id;

@Column(name = "name")
private String name;

@Column(name = "duration")
private String duration;

@Column(name = "description")
private String description;


@Column(name = "scope_career")
private String scopeCareer;

@Column(name = "status")
private Boolean status;

@CreationTimestamp
@Temporal(TemporalType.TIMESTAMP)
@JsonIgnoreProperties(allowGetters = true)
@Column(name = "create_datetime", nullable = false, updatable = false)
private Date createDateTime;

@UpdateTimestamp
@Temporal(TemporalType.TIMESTAMP)
@JsonIgnoreProperties(allowGetters = true)
@Column(name = "update_datetime", nullable = false)
private Date updateDateTime;

}
