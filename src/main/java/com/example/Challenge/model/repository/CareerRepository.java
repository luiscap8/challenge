package com.example.Challenge.model.repository;

import com.example.Challenge.model.entity.Career;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CareerRepository extends JpaRepository<Career, Long> {

}
