package com.example.Challenge.dto;

import lombok.Data;

@Data
public class NewCommentDto {

private Integer id;
private String comment;
private String subject;
private String priority;

}
