package com.example.Challenge.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CareerDto {
private Long id;
private String name;
private String duration;
private String description;
private String scopeCareer;
private Boolean status;
}
