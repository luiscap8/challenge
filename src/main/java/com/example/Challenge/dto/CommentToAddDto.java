package com.example.Challenge.dto;

import lombok.Data;
@Data
public class CommentToAddDto {
    private CommentToApiExternaDto ticket;

    public CommentToAddDto(NewCommentDto newCommentDto){
        this.ticket = new CommentToApiExternaDto(newCommentDto.getPriority(),
                newCommentDto.getSubject(), newCommentDto.getComment());
    }
}
