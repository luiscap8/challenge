package com.example.Challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import java.util.Arrays;
import java.util.Date;

@Data
public class CommentBodyDto {
	private Long id;
	private String type;
	@JsonProperty("request_id")
	private Integer requestId;
	private String body;
	@JsonProperty("html_body")
	private String htmlBody;
	@JsonProperty("plain_body")
	private String plainBody;
	@JsonProperty("public")
	private Boolean publiC;
	@JsonProperty("author_id")
	private Long authorId;
	private Arrays uploads;
	@JsonProperty("created_at")
	private Date createdAt;

}
