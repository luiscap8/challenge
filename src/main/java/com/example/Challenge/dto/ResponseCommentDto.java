package com.example.Challenge.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResponseCommentDto {

private List<CommentBodyDto> comments;

}
