package com.example.Challenge.dto;

import lombok.Data;

@Data
public class CommentToApiExternaDto {

    private String priority;
    private String subject;
    private BodyCommentDto comment;

public CommentToApiExternaDto(String priority, String subject, String comment) {

    this.priority = priority;
    this.subject = subject;
    this.comment = new BodyCommentDto(comment);
}
}
