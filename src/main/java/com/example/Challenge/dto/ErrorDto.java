package com.example.Challenge.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDto {
	private String descriptionError;
	private String message;
}
