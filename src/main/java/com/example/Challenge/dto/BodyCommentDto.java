package com.example.Challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class BodyCommentDto {
    private String body;
}
