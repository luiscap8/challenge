package com.example.Challenge.service.impl;

import com.example.Challenge.dto.CareerDto;
import com.example.Challenge.mapper.CareerMapper;
import com.example.Challenge.model.entity.Career;
import com.example.Challenge.model.repository.CareerRepository;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CareerServiceImplTest {
    @Mock
    private CareerRepository careerRepository;
    @Mock
    private CareerMapper careerMapper;
    @InjectMocks
    private CareerServiceImpl careerService;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    void getAllCareer() {
        final Career career1 = new Career(1L, "Ingeniería de Software", "test", "informatica", "legal", true, new Date(), new Date());
        final Career career2 = new Career(2L, "Ciencias de la Computación", "test", "informatica", "legal", true, new Date(), new Date());
        final CareerDto careerDto1 = new CareerDto(1L, "Ingeniería de Software", "test", "informatica", "legal", true);
        final CareerDto careerDto2 = new CareerDto(2L, "Ciencias de la Computación", "test", "informatica", "legal", true);

        List<Career> careers = Arrays.asList(career1, career2);
        when(careerRepository.findAll()).thenReturn(careers);
        List<CareerDto> careerDtos = Arrays.asList(careerDto1, careerDto2);
        when(careerMapper.toSimpleCareerDtoList(careers)).thenReturn(careerDtos);
        List<CareerDto> result = careerService.getAllCareer();
        assertEquals(2, result.size());
        assertEquals(careerDto1, result.get(0));
        assertEquals(careerDto2, result.get(1));
    }

    @Test
    void saveCareer() {
        final CareerDto careerDto = new CareerDto();
        final Career career = new Career();
        when(careerMapper.toSimpleCareer(careerDto)).thenReturn(career);
        careerService.saveCareer(careerDto);
        verify(careerRepository, times(1)).save(career);
    }

    @Test
    void updateCareer() {
        final Career career = new Career(1L, "Ingeniería de Software", "test", "informatica", "legal", true, new Date(), new Date());
        final CareerDto careerDto = new CareerDto(1L, "Ingeniería de Software", "test", "informatica", "legal", true);


        Mockito.when(careerRepository.findById(1L)).thenReturn(Optional.of(career));
        Mockito.when(careerMapper.updateCareerToSave(careerDto, career)).thenReturn(career);

        CareerServiceImpl careerService = new CareerServiceImpl(careerRepository, careerMapper);
        careerService.updateCareer(careerDto);
        Mockito.verify(careerRepository, Mockito.times(1)).findById(1L);
        Mockito.verify(careerRepository, Mockito.times(1)).save(career);
    }

    @Test
    public void downCareer() {
        final Long careerId = 1L;
        Career career = new Career();
        career.setId(careerId);
        career.setStatus(true);
        when(careerRepository.findById(careerId)).thenReturn(Optional.of(career));
        careerService.downCareer(careerId);
        verify(careerRepository, times(1)).save(career);
        assertFalse(career.getStatus());
    }

}