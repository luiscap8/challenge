<h1>Challenge</h1>

Una aplicación backend con Java que consume una API externa y realiza operaciones CRUD en una base de datos.

<h2>Características</h2>

Consume una API externa y devuelve su resultado.
Realiza operaciones CRUD (Crear, Leer, Actualizar y Eliminar) en una base de datos.

<h2>Cómo utilizar</h2>

1. Clonar el repositorio.
2. Importar el proyecto en un IDE compatible con Java (por ejemplo, IntelliJ o Eclipse).
3. Instalar plugin Lombok en el IDE.
4. Las credenciales de la base de datos en application.properties.
5. Las credenciales para zendesk se encuentran también en application.properties.
6. Ejecutar la aplicación.

<h2>Endpoints</h2>

1. HTTP request method GET <strong>  /api/v1/comment/{ticketId}  </strong>: Devuelve los comentarios obtenidos de la
   API externa con el ID del
   ticket en formato json.

2. HTTP request method PUT <strong>  /api/v1/comment  </strong>: Crea un nueva comentario en el ticket.

        Estructura para ingresar un camentario: Objeto json con tres propiedades comment, subject, priority

        comment : String
        subject: String
        priority : String           value: "urgent", "high", "normal", or "low".

        Ejemplo del objeto:
                {
                    "comment":"ejemplo",
                    "subject":"challenge",
                    "priority":"urgent"
                }


3. HTTP request method GET <strong>  /api/v1/careers  </strong>: Devuelve todas las carreras registradas en formato
   json.

4. HTTP request method POST <strong>  /api/v1/career  </strong>: Crea una nueva carrera en la tabla Career.

        Estructura para ingresar una carrera.

        name : String
        duration: String
        description : String
        scopeCareer : String
        status : Boolean                  value: "true", "false".

        Ejemplo del objeto:
                {
                    "name":"Ingeniera Informatica",
                    "duration":"3 años",
                    "description":"La carrera de informática es una disciplina que se enfoca en el estudio de la tecnología de la información y la comunicación.",
                    "scopeCareer":"area informatica",
                    "status":"true"
                }

5. HTTP request method PUT <strong>  /api/v1/career  </strong>: Actualiza campos determinados de la carrera con
   el ID especificado.

        Estructura para modificar una carrera.

        id  :  Long
        name : String
        duration: String
        description : String
        scopeCareer : String
        status : Boolean                  value: "true", "false".

        Ejemplo del objeto:
                {
                    "id": 1
                    "name":"Ingeniera Informatica",
                    "duration":"3 años",
                    "description":"La carrera de informática es una disciplina que se enfoca en el estudio de la tecnología de la información y la comunicación.",
                    "scopeCareer":"area informatica"
                    "status":"true"
                }

6. HTTP request methods DELETE <strong>  /api/v1/{careerId}/career  </strong>: Elimina la carrera de la tabla items
   con el ID especificado.

<h2>Tecnologías utilizadas</h2>

Java version 11

Spring Boot

JPA

H2

Maven

Lombok

Junit

Mockito

Mapstruct

<h2>Autor</h2>
Luis Enrique Carrillo